models/ctf/glow_red
{
 {
	map models/ctf/glow_red.tga
	tcMod scale 0.03 0.03
	tcMod scroll 0.1 -0.04
	tcMod rotate 0.1
 }
}
models/ctf/glow_blue
{
 {
	map models/ctf/glow_blue.tga
	tcMod scale 0.03 0.03
	tcMod scroll 0.1 -0.04
	tcMod rotate 0.1
 }
}