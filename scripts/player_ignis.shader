ignis
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/ignis.tga
		rgbgen lightingDiffuse
	}
}

ignishead
{
	dpreflectcube cubemaps/default/sky
 	{
		map textures/ignishead.tga
		rgbgen lightingDiffuse
	}
}
