0
\XonoticSingleplayerDialog\ Empieza una campaña de un solo jugador o acción instantánea de batalla contra bots


\XonoticMultiplayerDialog\Juega online, contra tus amigos en LAN, ver demos o cambia la configuración del jugador
\XonoticMultiplayerDialog/Servidores\Encuentra servidores para jugar
\menu_slist_showempty\Mostrar servidores vacíos
\menu_slist_showfull\Mostrar los servidores que no tienen espacio disponible
\net_slist_pause\Pausa la actualización de la lista de servidores para evitar que salteen
\XonoticMultiplayerDialog/Info...\Mostrar mas información sobre el actual servidor resaltado
\XonoticMultiplayerDialog/Marcador\Marcar el actual servidor resaltado para que sea mas facil encontrarlo en un futuro
\XonoticMultiplayerDialog/Crear\Crear tu propio juego
\XonoticMultiplayerDialog/Demos\Navegar y ver demos
\XonoticMultiplayerDialog/Player Setup\Configuración de jugador

\XonoticTeamSelectDialog/Unirse al'mejor' equipo (seleccion automática)\Auto seleccionar equipo (recomendado)
\XonoticTeamSelectDialog/rojo\Unirse al equipo rojo
\XonoticTeamSelectDialog/azul\Unirse al equipo azul
\XonoticTeamSelectDialog/amarillo\Unirse al equipo amarillo
\XonoticTeamSelectDialog/rosa\Unirse al equipo rosa

\timelimit_override\Límite de tiempo en minutos que cuando pase, terminara el combate
\fraglimit_override\Cantidad de puntos necesarios antes de que termine el combate
\menu_maxplayers\La máxima cantidad de jugadores o bots que pueden conectarse a tu servidor a la vez
\bot_number\Cantidad de bots en tu servidor
\skill\Especificar que experiencia tendran los bots
\g_maplist_votable\Numero de mapas que seran mostrados en en voto dde mapas al final de un combate
\sv_vote_simple_majority_factor\La mayoria gana
\XonoticMultiplayerDialog/Advanced settings...\Configuración avanzada ddel servidor
\XonoticMultiplayerDialog/Mutators...\Mutators
\g_dodging\Activar esquivado
\g_cloaked\Todos los jugadores son casi invisibles
\g_footsteps\Activar sonidos de pasos
\g_midair\solo es posible dañar a tu enemigo mientras este en el aire
\g_vampire\Daño dado a tu enemigo sera añadido a tu propia vida
\g_bloodloss\Cantidad de vida sera afectada por perdida de sangre
\sv_gravity\Hace que las cosas caigan lentamente al suelo, un valor bajo significa baja gravedad
\g_grappling_hook\Los jugadores aparecen con un gancho para aferrarse
\g_jetpack\Los jugadores aparecen con el jetpack
\g_pinata\Los jugadores dejan todas las armas cuando mueren
\g_weapon_stay\Las armas quedan despues de que son tomadas
\g_weaponarena\Seleccionando un arma, dara a todos los jugadores cual arma se eligió asi como infinita munición, y deshabilita cualquier otra toma de arma.
\g_instagib\Los jugadores tendran Minstanex, el cual es un railgun con daño infinito. Si el jugador queda sin munición, tendra 10 segundos para buscar mas o morira. El modo de disparo secundario es un laser que no inflige daño y es bueno para hacer bromas.
\g_nix\Xonotic sin items - en vez de recoger items, todos juegan con la misma arma. Despues de algún tiempo, comienza una cuenta regresiva, despues del cual todos juegan con otra arma.
\g_nix_with_laser\Siempre lleva el láser como arma adicional en Nix
\XonoticMultiplayerDialog/Select all\Seleccionar todos los mapas
\XonoticMultiplayerDialog/Select none\Deseleccionar todos los mapas


\XonoticMultiplayerDialog/Demo temporizado\Prueba cuan rápido tu computadora puede correr la demo seleccionada

\fov\Campo de visión en grados de 60 a 130, 90 es el default
\cl_bobcycle\Frecuencia de balanceo de la vista
\cl_zoomfactor\Cuan grande es el factor de zoom cuando la tecla de zoom es presionada
\cl_zoomsensitivity\Cuanto el zoom cambia la sensibilidad, desde 0 (baja sensibilidad) a 1 (sin cambio de sensibilidad)
\cl_zoomspeed\Cuan rápido es la vista ampliada, deshabilitar para un zoom instantáneo
\XonoticMultiplayerDialog/Weapon settings...\Seleccionar tu arma preferida, cambio automático y modelo de arma

\cl_weaponpriority_useforcycling\Haz uso de la lista de abajo when cambia de arma con la rueda del raton
\cl_autoswitch\Cambia automáticamente al arma recogida si es mejor que la que esta llevando
\r_drawviewmodel\Muestra el modelo de arma
\cl_gunalign\Posición del modelo de arma; requiere conexión

\crosshair_per_weapon\Configura diferentes miras para cada arma, esto ayuda si estas jugando sin un modelo de arma
\crosshair_color_override\Tambien configura el color del punto de mira dependiendo del arma que uses actualmente
\crosshair_size\Ajusta el tamaño del punto de mira
\crosshair_color_alpha\Ajusta la opacidad del punto de mira
\crosshair_color_red\Componente rojo del color del punto de mira
\crosshair_color_green\Componente verde del color del punto de mira
\crosshair_color_blue\Componente azul del color del punto de mira
\sbar_hudselector\Usar el viejo diseño de HUD
\XonoticMultiplayerDialog/Waypoints setup...\-
\_cl_name\Nombre con el cual aparecerás en el juego

\XonoticSettingsDialog\Cambiar la configuración del juego
\XonoticCreditsDialog\Los créditos de Xonotic
\XonoticTeamSelectDialog\-
\XonoticMutatorsDialog\-
\XonoticMapInfoDialog\-
\XonoticUserbindEditDialog\-
\XonoticWinnerDialog\-
\XonoticWeaponsDialog\-
\XonoticRadarDialog\-
\XonoticServerInfoDialog\-
\XonoticCvarsDialog\-

\XonoticQuitDialog\Salir del juego
\XonoticQuitDialog/Si\Volver al trabajo...
\XonoticQuitDialog/No\Tengo algunos puntos más por hacer!

\XonoticSettingsDialog/Entrada\configuración de entrada
\sensitivity\Multiplicador de velocidad del ratón
\menu_mouse_speed\Multiplicador de velocidad del raton en el menu, esto no afecta al apuntar en el juego
\m_filter\Suaviza el movimiento del raton, pero hace menos sensible al apuntar al objetivo
\m_pitch\Invierte el movimiento del raton en el eje Y
\vid_dgamouse\Hace uso de la entrada DGA del raton
\con_closeontoggleconsole\Permite el fijar un lazo de la consola para tambien cerrarla
\sbar_showbinds\Display actions / Teclas vinculadas en la cadena se mostrará en el juego
\cl_showpressedkeys\Mostrar el movimiento de teclas que el jugador esta presionando

\XonoticSettingsDialog/Video\configuración de video
\vid_width\Resolución de pantalla
\vid_bitsperpixel\Cuantos bits por pixel (BPP) para renderizar, 32 es lo recomendado
\vid_fullscreen\Habilitar el modo de pantalla completa (habilitado por defecto)
\vid_vsync\Habilitar sincronización vertical para prevenir rasgaduras en la imagen, limitara los fps a la tasa de refresco del monitor (desactivado por defecto)
\r_glsl\Habilitar sombreado de pixel de OpenGL 2.0 (activado por defecto)
\gl_vbo\Haz uso de objetos de búfer de vertices para almacenar geometría estática en la memoria de video para acelerar el renderizado (Vertices y Triangulos por defecto)
\r_depthfirst\Eliminar sobre-dibujado renderizando una version de única profundidad de la escena antes de que comience la renderización normal (desactivado por defecto)
\gl_texturecompression\Comprimir las texturas para tarjetas de video con poca cantidad de memoria disponible (ninguno por defecto)
\gl_finish\Hacer que la CPU wait esperen que la gpu termine cada marco, puede ayudar con algunas extrañas entradas o retraso de video en algunas máquinas (desactivado por defecto)
\v_brightness\Brillo en negros (por defecto: 0)
\v_contrast\Brillo en blancos (por defecto: 1)
\v_gamma\Valor de corrección de gama inverso, un efecto de brillo que no afecta a blancos o negros (por defecto: 1.125)
\v_contrastboost\Por cuanto multiplicar el contraste en areas oscuras (por defecto: 1)
\r_glsl_saturation\Ajuste de saturación (0 = escala de grises, 1 = normal, 2 = sobresaturado), requiere un control de color (por defecto: 1)
\v_glslgamma\Abilita el uso de GLSL para aplicar en la corrección gama, note que esto podría disminuir mucho el rendimiento (default: disabled)
\r_ambient\iluminación del ambiente, si se configura en muy elevado, tiende a hacer luz en los mapas de imagen aburrida y plana (por defecto: 4)
\r_hdr_scenebrightness\Brillo del renderizador global (por defecto: 1)
\vid_samples\activar antialiasing, el cual suaviza los bordes en geometrias en 3D. Note que esto puede disminuir bastante el rendimiento (por defecto: desactivado)
\v_flipped\Invertir la imagen horizontalmente (por defecto: desactivado)

\XonoticSettingsDialog/Efectos\configuración de efectos.
\r_subdivisions_tolerance\Cambiar la suavidad de las curvas en el mapa (por defecto: normal)
\gl_picmip\Cambiar la dureza de las texturas. Bajándolo efectivamente reducira el uso de la memoria de la textura, pero hará que las texturas aparezcan muy borrosas. (por defecto: bueno)
\r_picmipworld\Si se activa, solo reduce la calidad de texturas de los modelos (activado por defecto)
\mod_q3bsp_nolightmaps\Use mapas de alta resolución, hara que se vea lindo pero reducira la memoria de video (activado por defecto)
\cl_particles_quality\Multiplicador de cantidad de partículas. Menos significa menos partículas, lo cual dara mejor rendimiento (por defecto: 1.0)
\r_drawparticles_drawdistance\Las partículas que se alejen de esto no se dibujarán (por defecto: 1000)
\cl_decals\Activar decals (agujeros de balas y sangre) (habilitado por defecto)
\r_drawdecals_drawdistance\Los decals que se alejen de esta distancia no se dibujarán (por defecto: 300)
\cl_decals_time\Tiempo en segundos antes de que los decals desaparezcan (por defecto: 2)
\cl_gentle\Reemplazar sangre y tripas por cosas que no tengan nada de gore (desactivado por defecto)
\cl_nogibs\Reduce la cantidad de tripas o lo remueve completamente (por defecto: muchos)
\v_kicktime\Cuanto tiempo se verá el ultimo daño (por defecto: 0)
\gl_texture_anisotropy\Calidad de filtrado anisotrópico (por defecto: 1x)
\r_glsl_deluxemapping\Usar efectos de iluminado por pixel (activado por defecto)
\r_shadow_gloss\Activar el uso de glossmaps en texturas soportandolo (activado por defecto)
\gl_flashblend\Activar luces dinámicas por renderizado de coronas en vez de iluminación dinámica real (desactivado por defecto)
\r_shadow_realtime_dlight\Activar renderizado de luces dinámicas como explosiones y luces de cohete (activado por defecto)
\r_shadow_realtime_dlight_shadows\Activar renderizado de sombras de luces dinamicas (desactivado por defecto)
\r_shadow_realtime_world\Activar renderizado de la iluminación del mundo en tiempo real en mapas que lo soporten. Note que esto puede tener un gran impacto en el rendimiento. (desactivado por defecto)
\r_shadow_realtime_world_shadows\Activar renderizado de sombras de luces del mundo en tiempo real (desactivado por defecto)
\r_shadow_usenormalmap\Activar el uso de sombreado direccional en texturas (activado por defecto)
\r_showsurfaces\Desactivar texturas completamente para hardware muy lento. Esto aumentara el rendimiento en gran medida, pero se verça muy feo. (desactivado por defecto)
\r_glsl_offsetmapping\Efecto de mapeado del desplazamiento, que hará texturas con mapas de relieve parecerse como resaltado de la superficie plana de 2D (desactivado por defecto)
\r_glsl_offsetmapping_reliefmapping\Alta calidad de mapeado de desplazamiento, el cual también tiene un gran impacto en el rendimiento (desactivado por defecto)
\r_water\Calidad de refleccion y refracción, tiene un gran impacto en el rendimiento en mapas de superficies reflectantes (desactivado por defecto)
\r_water_resolutionmultiplier\Resolución de reflecciones/refracciones (por defecto: bueno)
\r_coronas\Habilitar iluminación en corona alrededor de ciertas luces (activado por defecto)
\r_coronas_occlusionquery\Coronas apagadas acorde a la visibilidad (activado por defecto)
\r_bloom\Activar efecto bloom, que ilumina los píxeles vecinos de píxeles muy brillantes.Tiene un gran impacto en el rendimiento. (desactivado por defecto)
\r_hdr\Versión de gran calidad de bloom, que tiene un gran impacto en el rendimiento. (desactivado por defecto)
\r_motionblur\Nivel de difuminado de movimiento - 0.5 recomendado
\r_damageblur\Cantidad de difuminado de movimiento en presencia de daños - 0.4 recomendado

\XonoticSettingsDialog/Sonido\configuración de audio
\mastervolume\-
\bgmvolume\-
\snd_staticvolume\-
\snd_channel0volume\-
\snd_channel3volume\-
\snd_channel6volume\-
\snd_channel7volume\-
\snd_channel4volume\-
\snd_channel2volume\-
\snd_channel1volume\-
\snd_speed\Frecuencia de salida del sonido
\snd_channels\Número de canales para la salida del sonido
\snd_swapstereo\Invertir canales izquierda o derecha
\snd_spatialization_control\Activar espacialización (mezcla ligeramente el canal derecho e izquierdo para disminuir la separación estereo un poco en los auriculares)
\cl_voice_directional\Activar voces direccionales
\cl_voice_directional_taunt_attenuation\Distancia desde el cual las burlas pueden escucharse
\cl_autotaunt\automáticamente burlarse del enemigo al anotar puntos
\cl_sound_maptime_warning\Sonido del anunciante que te indica el tiempo restante del combate
\cl_hitsound\Reproduce un sonido indicador de anotacion cuando disparas a un enemigo
\menu_sounds\Reproduce sonidos cuando se clickea o se posiciona sobre un item del menú

\XonoticSettingsDialog/Red\configuración de la red
\cl_movement\Activar predicción de movimiento del lado del cliente
\cl_nolerp\Activar actualización suave en la red
\shownetgraph\Activar un gráfico de tamaño de paquetes y otra información
\_cl_rate\Especificar tu velocidad de conección con este separador
\cl_netfps\Cuantos paquetes de entrada mandar a un servidor por cada segundo
\cl_curl_maxdownloads\Número máximo de descargas concurrentes de HTTP/FTP
\cl_curl_maxspeed\Velocidad de descarga máxima
\cl_port\Forzar al cliente a usar un puerto elegido a menos que se establezca en 0

\XonoticSettingsDialog/Misc\configuración misc
\showtime\Mostrar la hora y el día actual, útil en capturas de pantalla
\showdate\Mostrar el día actual, útil en capturas de pantalla
\showfps\Mostrar tus marcos/frames por segundo
\cl_showspeed\Mostrar la velocidad del jugador
\cl_showspeed_unit\Seleccionar la velocidad del velocímetro. qu/s = in/s
\cl_showacceleration\Mostrar la aceleración del jugador
\cl_showacceleration_scale\Cambiar el acelerómetro por este multiplicador de escala

\XonoticSettingsDialog/configuración avanzada...\configuracion avanzada donde puedes ajustar cada variable del juego
\g_friendlyfire\Porcentaje de daño infligido a tus compañeros de equipo
\g_mirrordamage\Porcentaje de daño de equipo que se reflejará a ti
\g_tdm_teams_override\Sobreescribir la cantidad de equipos por defecto de los modos en equipo

\cl_teamradar_position\-
\cl_teamradar_size\-
\cl_teamradar_zoommode\-
\cl_teamradar_rotation\-
\cl_teamradar_scale\-
\cl_teamradar_foreground_alpha\-
\cl_teamradar_background_alpha\Valor de opacidad del radar de fondo
\viewsize\Enable/Desavilitar el HUD de fondo
\sbar_alpha_bg\alor de opacidad del HUD de fondo
\sbar_color_bg_r\Componente rojo del HUD de fondo
\sbar_color_bg_g\Componente verde del HUD de fondo
\sbar_color_bg_b\Componente azul del HUD de fondo
\sbar_color_bg_team\Saturacion del color de equipo del HUD de fondo
\cl_hidewaypoints\Mostrar indicadores de ubicación/caminos específicos de diferentes tipos de juego
\g_waypointsprite_scale\Escala del multiplicador de los indicadores de ubicación/caminos
\g_waypointsprite_alpha\Transparencia del control de los indicadores de ubicación/caminos
\cl_shownames\Mostrar el nombre del jugador al que estás apuntando

\crosshair_hittest\Ninguno: no hacer pruebas de aciertos para el punto de mira; Apuntado: difuminar la punta de mira cuando no se apunta contra la pared; Enemigos: también ampliar el punto de mira cuando aciertas al enemigo
